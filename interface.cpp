#include"matrix.h"

double identity_mat(int i, int j)
{
	if (i == j) return 1;
	return 0;
}

double gilbert_mat(int i, int j)
{
    //put your mastrix here
    return abs(i-j);
}

void read_from_file(int *n, double **a, FILE *f)
{

	//first we should read the total length
	try {
        if(fscanf(f, "%d", n) != 1) {
            std::cerr<<"Violated file\n";
            throw(-1);
        }
		*a = new double[*n* *n];

		for (int i = 0; i < *n; i++) {
			for (int j = 0; j < *n; j++) {
                if(fscanf(f, "%lf", *a + i* *n + j) != 1) {
                    std::cerr<<"Violated file\n";
                    throw(-1);
                }
			}
		}
	}
	catch (...) {
		std::cerr << "The information in file does not satisfies the required format.\n";
	}
}

void formula_calc(int n, double *a, double(*formula)(int i, int j)) {
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++) a[n * i + j] = (*formula)(i, j);
}



void write(int n, int m, double *a, std::ostream &f)
{
	int border = (n < m) ? n : m;
	f << "\n=============================================================\n";
	for (int i = 0; i < border; i++) {
		for (int j = 0; j < border; j++) {
			f << std::fixed << std::setw(16) << a[n * i + j] << "   ";
		}
		f << "\n\n";
	}
	f << "\n=============================================================\n";
}
