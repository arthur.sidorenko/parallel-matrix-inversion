#include "matrix.h"
#include<string.h>

using namespace std;


int main(int argc, char **argv) {
    pthread_mutex_t tr_mut;
    pthread_barrier_t bar1;
    int attempts = 3; //number of experiments are going to be made
	//chhose the way to take the matrix
	cout << "Hello\n";
	FILE *f = nullptr;
	int n;
	const int corner_size = 6;
	double *a, *ans, *aux;
	if (argc <= 1) {
		std::cerr << "No parameters have been given\n";
		return -1;
	}
    //reading input
	bool file_input = (!strcmp(argv[1], "F"));
	if (file_input) {
		//reading from file
        if ((f=fopen(argv[2], "r")) == 0)  {
			std::cerr << "The file hasn't been opened\n";
            std::cout << argv[2] << "\n";
			return -1;
		}
		read_from_file(&n, &a, f);
        //set attempts
        if(argc > 3) {
            if ( sscanf(argv[3], "%d", &attempts) != 1) {
                std::cerr << "Violated format of the input\n";
                return -1;
            }
            if(attempts > 20) {
                std::cerr <<"You've offered too many attempts";
                attempts = 20;
            }
        }
	}
    //calculatng matrix by formula
	else {
        //set size
        if ( sscanf(argv[1], "%d", &n) != 1) {
			std::cerr << "Violated format of the input\n";
			return -1;
		};
        //set number of attempts
        if(argc > 2) {
            if ( sscanf(argv[2], "%d", &attempts) != 1) {
                std::cerr << "Violated format of the input\n";
                return -1;
            }
            if(attempts > 20) {
                std::cerr <<"You've offered too many attempts";
                attempts = 20;
            }
        }
		a = new double[n*n];
		formula_calc(n, a, *gilbert_mat);

	}
	ans = new double[n*n];
	aux = new double[n];


	write(n, corner_size, a);

    //the array ex contains results of "experiments"
	examine *ex = new examine[attempts];
    thread_data *dat = NULL; //this array contains data for threads
	double res;
    for (int thread_num = 1, count = 0; count < attempts; thread_num *= 2, count++) {
		std::cout << "Examination with " << thread_num << " threads\n\n";
		if (n < thread_num) {
			thread_num = n;
			std::cout << "Since n < threads, the total number of threads has been reduced\n";
		}
        double t1 = 0, t2 = -1;
		pthread_t* threads = new pthread_t[thread_num];
		if (pthread_mutex_init(&tr_mut, NULL) != 0) {
			cerr << "Mutex error\n";
			return -1;
		}
		if (pthread_barrier_init(&bar1, NULL, thread_num) != 0) {
			cerr << "Barrier error\n";
			return -1;
		}
		try {
            t1 = get_wall_time();
			//create new threads
            dat = new thread_data[thread_num];
            for(int i = 0; i < thread_num; i++)
                dat[i] = thread_data(n, thread_num, a, ans, aux, i, &tr_mut, &bar1);
			for (int i = 0; i < thread_num; i++) {
                if (pthread_create(threads + i, NULL, &par_inv_func, dat + i) != 0) {
					std::cerr << "Unexpected thread error\n";
					return -1;
				}
			}
			for (int i = 0; i < thread_num; i++) pthread_join(threads[i], NULL);
			t2 = get_wall_time();
			std::cout << "THe inversion took " << t2 - t1 << " sec. \n";
            delete [] dat;

		}
		catch (singular_matrix &ups) {
			std::cerr << ups.what() << "\n";
            //for(int i = 0; i < thread_num; i++) pthread_kill(threads[i], -1);
			if (file_input) fclose(f);
			delete[] a;
			delete[] ans;
			delete[] aux;
            delete[] ex;
            delete[] dat;
            delete[] threads;
			return -1;

		}
		catch (...) {
			std::cerr << "A critical error occured.\n";
		}
		std::cout << "\nThe inverted matrix is that: \n";
		write(n, corner_size, ans);

		if (file_input) {
			rewind(f);
			delete[] a;
			read_from_file(&n, &a, f);
		}
		else {
			formula_calc(n, a, *gilbert_mat);
		}
		res = residual(n, a, ans);
		std::cout << "The residual is equal to " << scientific << res  << "\n";
		delete[] threads;
		pthread_mutex_destroy(&tr_mut);
		pthread_barrier_destroy(&bar1);
		ex[count].residual = res;
		ex[count].time = t2 - t1;
	}
	
	std::cout << "Final results: \n";
	std::cout << "Threads        Time(quotient)       residual\n";
    for (int thread_num = 1, count = 0; count < attempts; thread_num *= 2, count++) {
		std::cout << std::fixed << std::setw(5) << thread_num << "             "
			<< std::fixed << std::setw(5) << setprecision(5) << ex[0].time / ex[count].time << "           "
            << std::fixed << std::setw(5) << setprecision(8) << scientific << ex[count].residual<<"\n";
	}
	if(file_input) fclose(f);
	delete[] a;
	delete[] ans;
	delete[] aux;
    delete[] ex;

	return 0;
}

double my_abs(double x) {
	if (x >= 0) return x;
	return -x;
}

double residual(int n, double *a, double *inv) {
	double cur = 0, max = 0;
	double elem = 0;
    double *p = a;
    transpose(n, inv);
    double *v = inv;
	for (int i = 0; i < n; i++) {
		cur = 0;
        v = inv;
		for (int j = 0; j < n; j++) {
			elem = 0;
            for (int k = 0; k < n; k++) {
                elem += p[k] * v[k];
            }
			if (i == j) elem -= 1;
			cur += my_abs(elem);
            v+=n;
		}
        p+=n;
		if (cur > max) max = cur;
	}
    transpose(n, inv);
	return max;
}

//it is to count wall time
//  Windows
#ifdef _WIN32
#include <Windows.h>
double get_wall_time() {
	LARGE_INTEGER time, freq;
	if (!QueryPerformanceFrequency(&freq)) {
		//  Handle error
		return 0;
	}
	if (!QueryPerformanceCounter(&time)) {
		//  Handle error
		return 0;
	}
	return (double)time.QuadPart / freq.QuadPart;
}


//  Posix/Linux
#else
#include <time.h>
#include <sys/time.h>
double get_wall_time() {
	struct timeval time;
	if (gettimeofday(&time, NULL)) {
		//  Handle error
		return 0;
	}
	return (double)time.tv_sec + (double)time.tv_usec * .000001;
}

#endif
