#include "matrix.h"
#include<string.h>

//==================inversion itself=====================

constexpr double sing_const = 10e-16;

void compose_identity_matrix(int n, double * a)
{
	formula_calc(n, a, &identity_mat);
}

double matrix_norm(int n, double *a) {
	double m = 0;
	for (int i = 0; i < n; i++) {
		double cur = 0;
		for (int j = 0; j < n; j++) {
			cur += my_abs(a[i * n + j]);
		}
		if (cur > m) m = cur;
	}
	return m;
}


void inverse(int n, double *a, double *ans, double *aux)
{
	//first of all, we need to transpose our matrix
	transpose(n, a);
	compose_identity_matrix(n, ans);
	double mat_norm = matrix_norm(n, a);

	//the main loop that leads through the whole matrix
	for (int i = 0, len = n; i < n; i++, len--) {
		//calculate the reflection vector
		double *p = a + i * n + i; //current piece of string pointer
		double *q = ans; //with the ans matrix, it sounds more difficult
		reflection_vector(len, p, aux, mat_norm * sing_const );
		//then we have to perform reflection for each string for both matricies a and ans simultaneously
		for (int k = 0; k < n - i; k++) {
			reflect(len, p + k * n, aux);
		}
		for (int k = 0; k < n; k++) {
			reflect(len, q + i + k * n, aux);
		}

	};
	transpose(n, a);
	transpose(n, ans);

	for (int i = n - 1; i >= 0; i--) {
		double divisor = a[i * n + i];
		div_string(n, a, i, divisor);
		div_string(n, ans, i, divisor);
		//std::cout << "qstep i = " << i << "\n";
		//write(n, 10, a); write(n, 10, ans);
		for (int k = i - 1; k >= 0; k--) {
			double mul = a[k * n + i];
			substract_string(n, a, k, i, mul);
			substract_string(n, ans, k, i, mul);
		}
	}
}

void div_string(int n, double *a, int s, double k)
{
	double *p = a + s * n;
	for (int i = 0; i < n; i++) p[i] = p[i] / k;
	
}

void substract_string(int n, double *a, int s1, int s2, double k)
{
	double *p = a + s1 * n;
	double *q = a + s2 * n;
	for (int i = 0; i < n; i++) p[i] -= q[i] * k;
}

double scal_prod(int k, double * v1, double * v2)
{
	double sum = 0;
	for (int i = 0; i < k; i++) sum += v1[i] * v2[i];
	return sum;
}

void reflection_vector(int k, double * v, double * aux, double singular_c)
{
	double norm = norm_of_string(k, v);
	if (my_abs(norm) <= singular_c) throw singular_matrix(); //it is the sign that there will be the number zero on the diagonal
	aux[0] = v[0] - norm;
	for (int i = 1; i < k; i++) aux[i] = v[i];
	//double ans_norm = norm_of_string(k, aux);
	//if ((ans_norm > 0)) for (int i = 0; i < k; i++) aux[i] /= ans_norm;
}

double norm_of_string(int k, double * v)
{
	double sum = 0;
	for (int i = 0; i < k; i++) sum += v[i] * v[i];
	return std::sqrt(sum);
}
double sqr_norm_of_string(int k, double * v)
{
	double sum = 0;
	for (int i = 0; i < k; i++) sum += v[i] * v[i];
	return sum;
}

void transpose(int n, double *a)
{
	for (int i = 0; i < n; i++) {
		for (int j = i + 1; j < n; j++) {
			swap_two_numbers(a + n * i + j, a + n * j + i);
		}
	}
}

void swap_two_numbers(double * a, double * b)
{
	double c = *a;
	*a = *b;
	*b = c;
}

void reflect(int k, double * v, double * refl)
{
	double q = sqr_norm_of_string(k, refl);
	//std::cout << q;
	if (!(q > 0)) return; 
	double prod = 2 * scal_prod(k, v, refl) / q;
	for (int i = 0; i < k; i++) v[i] -= prod * refl[i];
}

bool flag_check(int k, bool *flag) {
    bool x = true;
    for(int i = 0; i < k; i++) {
        x = flag[i];
        if(x == false) return false;
    }
    return true;
}

void *par_inv_func(void *data) {
	thread_data *data2 = (thread_data*)data;
	int n = data2->n;
	double *a = data2->a, *aux = data2->aux, *ans = data2->ans;
    bool *flag = data2->flag;
    int thread_num = data2->thread_num;

	int id = data2->row;
    flag[id] = true;
	pthread_barrier_t *bar = data2->bar;

	//first of all, we need to transpose our matrix
    if (id == 0) {
        transpose(n, a);
        compose_identity_matrix(n, ans);
		//std::cout << "In func, n = " << n << "\n";
    }
    //double mat_norm = matrix_norm(n, a);
	pthread_barrier_wait(bar);

	int begin_ans = job_calc(n, thread_num, id);
	int end_ans = job_calc(n, thread_num, id + 1) - 1;
	int begin_a, end_a;
	//the main loop that leads through the whole matrix
	for (int i = 0, len = n; i < n; i++, len--) {
		//calculate the reflection vector
		double *p = a + i * n + i; //current piece of string pointer
		double *q = ans; //with the ans matrix, it sounds more difficult
		if (id == 0) {
            try {
            reflection_vector(len, p, aux, sing_const);
            } catch(singular_matrix ups) {
                std::cout << ups.what() <<"\n";
                flag[id] = false;
            }
		}
		pthread_barrier_wait(bar);
        if(flag_check(thread_num, flag) == false) exit(3);
        pthread_barrier_wait(bar);
		//then we have to perform reflection for each string for both matricies a and ans simultaneously
        begin_a = (n-i)*id/thread_num;
        end_a = (n-i)*(id+1) / thread_num - 1;
		for (int k = begin_a; k <= end_a; k++) {
			reflect(len, p + k * n, aux);
		}
		for (int k = begin_ans; k <= end_ans; k++) {
			reflect(len, q + i + k * n, aux);
		}
		pthread_barrier_wait(bar);
	};
    //pthread_barrier_wait(bar);
	if (id == 0) {
		transpose(n, a);
		transpose(n, ans);
	}
	pthread_barrier_wait(bar);
    //std::cout << "Reflected\n";
	for (int i = n - 1; i >= 0; i--) {
		//std::cout << "poo\n";
		if (id == 0) {
			double divisor = a[i * n + i];
			div_string(n, a, i, divisor);
			div_string(n, ans, i, divisor);
		}
		pthread_barrier_wait(bar);

        begin_a = (i)*id/thread_num;
        end_a = (i)*(id+1) / thread_num - 1;
        //pthread_barrier_wait(bar);
		if(i>0) for (int k = begin_a; k <= end_a; k++) {
			double mul = a[k * n + i];
			substract_string(n, a, k, i, mul);
			substract_string(n, ans, k, i, mul);
		}
		pthread_barrier_wait(bar);
	}
	return NULL;
}

int job_calc(int n, int thread_num, int id) {
	return (id * n) / thread_num;
}
