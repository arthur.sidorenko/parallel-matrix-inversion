#include<iostream>
#include<cstdlib>
#include<stdio.h>
#include<ctime>
#include<string>
#include<iomanip>
#include<cmath>
#include<exception>

#include<pthread.h>

//singular matrix exception
class singular_matrix {
public:
	std::string ups;
	singular_matrix(): ups("The matrix is singular\n") {};
	const std::string &what() {return ups;};
};

struct thread_data {
	int n;
	int row;
	int thread_num;
	double *a, *ans, *aux;
	pthread_mutex_t *mut;
	pthread_barrier_t *bar;
	bool *flag;
	thread_data(int n_, int thread_num_, double *a_, double *ans_, double* aux_, int row_, 
		pthread_mutex_t *mut_, pthread_barrier_t *bar_, bool *flag_) {
	  n = n_; thread_num = thread_num_; a = a_; ans = ans_; aux = aux_; row = row_;
	  mut = mut_; bar = bar_; flag = flag_;
	}
	thread_data() {};
	
};

struct examine {
	double time, residual;
};

//function to assign job to each thread
int job_calc(int n, int thread_num, int id);

//several matrix formulae
double identity_mat(int i, int j);
double gilbert_mat(int i, int j);
//double* diagonal_mat(int n, double *v); // v contains coefficients that will be on the diagonal
//double* anti_diagonal_mat(int n, double *v); // v contains coefficients that will be on the counter-diagonal
//double random_mat(int n);

//reading
void read_from_file(int *n, double **a, FILE *f);
//void formula_query(int *n, double **a, mat_location &q);
void formula_calc(int n, double *a, double (*formula)(int i, int j));

//writing
void write(int n, int m, double *a, std::ostream &f = std::cout);

//performing calculations
void compose_identity_matrix(int n, double *a);
void inverse(int n, double *a, double *ans, double *aux); //performes inversion of the matrix
void div_string(int n, double *a, int s, double k); //divides the string s of the matrix a to the nimber k
void substract_string(int n, double *a, int s1, int s2, double k); // substrates string s2 from string s1
double scal_prod(int k, double *v1, double *v2);
void reflection_vector(int k, double *v, double *aux, double singular_c); //find out the reflection vector
double norm_of_string(int k, double *v); //calculates Euclidean norm of the string
double sqr_norm_of_string(int k, double * v);
void transpose(int n, double *a); //transposes a matrix
void swap_two_numbers(double *a, double *b);
void reflect(int k, double *v, double *refl); //performes reflection for vector v through a hyperplane, otrhogonal to a
double matrix_norm(int n, double *a); //this is to calculate the matrix norm
//matrix norm
double residual(int n, double *a, double *inv);
double my_abs(double x);

//for parallel calculations
void *par_inv_func(void *dat);
examine paprallel_organise(int n, int thread_num, double *a, double *ans, double *aux);

//timing
double get_wall_time();